import torch as th
import torch.nn as nn

import torch.autograd as th_autograd


class DiscBlock(nn.Sequential):
    def __init__(
            self,
            input_channels: int,
            output_channels: int
    ):
        super(DiscBlock, self).__init__(
            nn.Conv2d(
                input_channels,
                output_channels,
                kernel_size=(3, 3),
                stride=(2, 2),
                padding=(1, 1)
            ),
            nn.LeakyReLU(2e-1)
        )


class Discriminator(nn.Sequential):
    def __init__(self):

        disc_layers = [
            (2, 8),
            (8, 16),
            (16, 24),
            (24, 32),
            (32, 40),
            (40, 48),
            (48, 56),
            (56, 64)
        ]

        super(Discriminator, self).__init__(*[
            DiscBlock(i, o)
            for i, o in disc_layers
        ])

        self.add_module(
            "flatten", nn.Flatten(start_dim=1, end_dim=-1)
        )

        out_size = 512 // 2 ** 8

        self.add_module(
            "clf", nn.Linear(
                out_size ** 2 * disc_layers[-1][1], 1
            )
        )

    def gradient_penalty(
            self,
            x_real: th.Tensor,
            x_gen: th.Tensor
    ) -> th.Tensor:
        device = "cuda" if next(self.parameters()).is_cuda else "cpu"

        batch_size = x_real.size()[0]
        eps = th.rand(batch_size, 1, 1, 1, device=device)

        x_interpolated = eps * x_real + (1 - eps) * x_gen

        out_interpolated = self(x_interpolated)

        gradients = th_autograd.grad(
            out_interpolated, x_interpolated,
            grad_outputs=th.ones(out_interpolated.size(), device=device),
            create_graph=True, retain_graph=True
        )

        gradients = gradients[0].view(batch_size, -1)
        gradients_norm = gradients.norm(2, dim=1)
        gradient_penalty = ((gradients_norm - 1.) ** 2.).mean()

        grad_pen_factor = 10.

        return grad_pen_factor * gradient_penalty


class GenBlock(nn.Sequential):
    def __init__(
            self,
            input_channels: int,
            output_channels: int,
            last_layer: bool
    ):
        super(GenBlock, self).__init__(
            nn.ConvTranspose2d(
                input_channels,
                output_channels,
                kernel_size=(3, 3),
                stride=(2, 2),
                padding=(1, 1),
                output_padding=(1, 1)
            ),
            nn.Tanh() if last_layer else nn.LeakyReLU(2e-1)
        )


class Generator(nn.Sequential):
    def __init__(self, rand_channels: int):

        gen_layers = [
            (rand_channels, 56),
            (56, 48),
            (48, 40),
            (40, 32),
            (32, 24),
            (24, 16),
            (16, 8),
            (8, 2)
        ]

        super(Generator, self).__init__(*[
            GenBlock(i, o, idx == len(gen_layers) - 1)
            for idx, (i, o) in enumerate(gen_layers)
        ])


def wasserstein_discriminator_loss(y_real: th.Tensor,
                                   y_fake: th.Tensor) -> th.Tensor:
    return -(th.mean(y_real) - th.mean(y_fake))


def wasserstein_generator_loss(y_fake: th.Tensor) -> th.Tensor:
    return -th.mean(y_fake)

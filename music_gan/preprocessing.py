import numpy as np

import scipy
from scipy.io.wavfile import read as read_wav
from scipy.signal import stft, istft


def read_raw_audio(wav_file: str) -> np.ndarray:
    sample_rate, raw_audio = read_wav(wav_file)

    # normalize signal
    raw_audio_float = raw_audio / np.iinfo(raw_audio.dtype).max

    # if stereo -> mono
    if len(raw_audio_float.shape) == 2:
        raw_audio_float = raw_audio_float.mean(axis=-1)

    return raw_audio_float


def raw_audio_to_magn_phase(raw_audio_mono: np.ndarray) -> np.ndarray:
    nperseg = 1024
    noverlap = 3 * nperseg // 4
    nfft = 1024

    # short time fourier transform
    f, t, z = stft(raw_audio_mono, nperseg=nperseg, noverlap=noverlap, nfft=nfft)

    # Nyquist
    z = z[:-1, :]

    # magnitude and phase
    m = np.abs(z)
    p = np.angle(z)

    # unwrap -> get rid of % 2*PI
    unwrapped_phase = np.unwrap(p)

    # instantaneous frequency / phase
    inst_phase = unwrapped_phase[:, 1:] - unwrapped_phase[:, :-1]
    m = m[:, 1:]

    # magnitude to log
    m = np.log(m + 1)

    # Normalize
    m = (m - m.min()) / (m.max() - m.min())
    inst_phase = (inst_phase - inst_phase.min()) \
                 / (inst_phase.max() - inst_phase.min())

    # Set to [-1; 1]
    m = 2. * m - 1.
    inst_phase = 2. * inst_phase - 1.

    return np.stack([m, inst_phase], axis=0)


def split_magn_phase(magn_phase: np.ndarray) -> np.ndarray:
    image_length = 512

    magn_phase = magn_phase[:, :, :magn_phase.shape[-1] - magn_phase.shape[-1] % image_length]

    nb_image = magn_phase.shape[-1] // image_length

    magn_phase = np.stack(np.split(magn_phase, nb_image, axis=-1), axis=0)

    return magn_phase


def magn_phase_to_wav(magn_phase: np.ndarray, wav_file: str) -> None:
    magn = (magn_phase[0, :, :] + 1.) / 2.
    inst_phase = (magn_phase[1, :, :] + 1.) / 2. * 2. * np.pi - np.pi

    unwrapped_phase = np.cumsum(inst_phase, axis=1)

    magn = np.exp(magn) - 1

    phase = unwrapped_phase % (2. * np.pi)

    r = magn * np.cos(phase)
    i = magn * np.sin(phase)

    r = np.concatenate((r, np.zeros((1, r.shape[1]))), axis=0)

    i = np.concatenate((i, np.zeros((1, i.shape[1]))), axis=0)

    z = r + i * 1j

    _, raw_audio = istft(
        z,
        nperseg=1024,
        noverlap=3 * 1024 // 4
    )

    scipy.io.wavfile.write(wav_file, 44100, raw_audio)
